d = {'name': 'Ivan', 'age': '20'}
second_d = d

print(id(d))
print(id(second_d))

d['newKey'] = 'newValue'
print(d)
print(second_d)