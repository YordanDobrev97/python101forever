some_variable = True

if some_variable is True:
    print('The value is true')

some_variable = False

if some_variable is not True:
    print('someting string is false')

dictionary = {}

if dictionary is not None:
    print('dictionary')

age = 22
if age > 18:
    print('go disco')
else:
    print('no go disco')

day = 'Monday'
if day == 'Monday':
    print(1)
elif day == 'Tuesday':
    print(2)
else:
    print('don\'t know')
