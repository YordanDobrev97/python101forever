def print_name(name='Default name'):
    print(name)


print_name('Pesho')
print_name('Gosho')
print_name('Ivan')
print_name()


name = 'Ivan'
age = 18


def say_age():
    name = 'Petur'
    print(f'I am {name} and I am {age} year old.')


say_age()


def sum1(*args):
    print(args)
    return sum(args)


print(sum1(1, 2, 3, 4))


def keys(**d):
    print(d)


keys(a=1, b='text...')
