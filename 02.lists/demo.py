numbers = [1, 2, 3]

for num in numbers:
    print(num)

names = ['Pesho', 'Gosho', 'Ivan']

for index in range(len(names)):
    print(index, names[index])

first_list = [1]
second_list = [1]

print(first_list == second_list)
print(1 in first_list)
