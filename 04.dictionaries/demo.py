dictionary = {'key': 'value'}

print(dictionary.get('key'))

print(dictionary == {})
print(dictionary is not None)

d = {
    'Ivan': 23,
    'Petur': 20,
    'Maria': 18
}

for x in d.items():
    print(x)
