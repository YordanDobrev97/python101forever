
def sum_of_digits(number):
    sum = 0
    number = abs(number)

    while number > 0:
        last_digit = int(number % 10)
        sum += last_digit
        number = int(number / 10)
    return sum


tests = [
    (1325132435356, 43),
    (123, 6),
    (6, 6),
    (-10, 1),
    (1420, 7)
]

for input, expected in tests:
    result = sum_of_digits(input)
    print(f'{result}, {expected}')
    print(result == expected)
