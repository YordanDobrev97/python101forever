def nan_expand(times):
    result = ''

    has_not = False
    for i in range(times):
        has_not = True
        result += 'Not a '

    if has_not:
        result += 'NaN'

    return result


tests = [
    (0, ''),
    (1, 'Not a NaN'),
    (2, 'Not a Not a NaN'),
    (3, 'Not a Not a Not a NaN')
]

for input, expected in tests:
    result = nan_expand(input)
    print(f'{result}, {expected}')
    print(result == expected)
