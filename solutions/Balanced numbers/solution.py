def is_number_balanced(number):
    number_as_string = str(number)
    if len(number_as_string) == 1:
        return True

    middle_index = len(number_as_string) // 2

    left_sum = 0
    is_including = middle_index if len(
        number_as_string) % 2 == 0 else middle_index + 1

    for index in range(0, is_including):
        number_value = int(number_as_string[index])
        left_sum += number_value

    right_sum = 0
    for index in range(middle_index, len(number_as_string)):
        number_value = int(number_as_string[index])
        right_sum += number_value

    return left_sum == right_sum


tests = [
    (9, True),
    (4518, True),
    (28471, False),
    (1238033, True)
]

for input, expected in tests:
    result = is_number_balanced(input)
    print(f'{result}, {expected}')
