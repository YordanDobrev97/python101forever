
def sum_matrix(matrix):
    total_sum = 0
    for array in matrix:
        total_sum += sum(array)
    return total_sum


tests = [
    ([[1, 2, 3], [4, 5, 6], [7, 8, 9]], 45),
    ([[0, 0, 0], [0, 0, 0], [0, 0, 0]], 0),
    ([[1, 2], [3, 4], [5, 6], [7, 8], [9, 10]], 55)
]

for input, expected in tests:
    result = sum_matrix(input)
    print(f'{result}, {expected}')
    print(result == expected)
