
def group(items):
    if len(items) == 0:
        return []

    previous_item = items[0]
    group_items = [[previous_item]]
    number_group = 0

    for index in range(1, len(items)):
        item = items[index]

        if previous_item == item:
            group_items[number_group].append(item)
        else:
            number_group += 1
            group_items.append([])
            group_items[number_group].append(item)
        previous_item = item

    return group_items


tests = [
    ([1, 1, 1, 2, 3, 1, 1], [[1, 1, 1], [2], [3], [1, 1]]),
    ([1, 2, 1, 2, 3, 3], [[1], [2], [1], [2], [3, 3]]),
    ([], []),
    ([1], [[1]]),
    ([1, 1, 1, 1], [[1, 1, 1, 1]])
]

for input, expected in tests:
    result = group(input)
    print(f'{result}, {expected}')
    print(result == expected)
