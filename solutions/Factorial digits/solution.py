def factorial(n):
    # 5! -> 5 * 4 * 3 * 2 * 1
    if n == 0:
        return 1
    if n == 1:
        return 1
    return n * factorial(n - 1)


def fact_digits(n):
    sum = 0

    while n > 0:
        digit = int(n % 10)
        fact_result = factorial(digit)
        sum += fact_result
        n = int(n / 10)
    return sum


tests = [
    (5, 120),
    (111, 3),
    (145, 145),
    (999, 1088640)
]

for input, expected in tests:
    result = fact_digits(input)
    print(f'{result}, {expected}')
    print(result == expected)
