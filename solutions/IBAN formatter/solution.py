import re

def iban_formatter(iban):
    iban = re.sub('[\s+]', '', iban)

    length = len(iban)
    start_index = 0
    end_index = 4
    max_symbols = 4

    final_iban = ""
    while length > 0:
        part = iban[start_index:end_index]
        final_iban = final_iban + part + " "
        length = length - max_symbols
        start_index = end_index
        end_index = end_index + max_symbols
    return final_iban


tests = [
    ('BG80BNBG96611020345678', 'BG80 BNBG 9661 1020 3456 78'),
    ('BG80 BNBG 9661 1020 3456 78', 'BG80 BNBG 9661 1020 3456 78'),
    ('BG14TTBB94005362446381', 'BG14 TTBB 9400 5362 4463 81'),
    ('BG91UNCR70001864961754', 'BG91 UNCR 7000 1864 9617 54')
]

for input, exptected in tests:
    result = iban_formatter(input)
    print(f'{result}, {exptected}')
    print(result.strip() == exptected)
