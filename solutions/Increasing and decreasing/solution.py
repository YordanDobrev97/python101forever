from enum import Enum


class Monotonicity(Enum):
    INCREASING = 1
    DECREASING = 2
    NONE = 3


def increasing_or_decreasing(ns):
    if len(ns) in [0, 1]:
        return Monotonicity.NONE

    previous_element = ns[0]

    current_state = None
    first_state = None
    start = 1
    for index in range(start, len(ns)):
        if first_state is not current_state:
            current_state = Monotonicity.NONE
            break
        value = ns[index]
        if previous_element < value:
            current_state = Monotonicity.INCREASING
        elif previous_element > value:
            current_state = Monotonicity.DECREASING
        else:
            current_state = Monotonicity.NONE
        previous_element = value

        if first_state is None:
            first_state = current_state

    if first_state is not current_state or current_state == None:
        current_state = Monotonicity.NONE

    return current_state


tests = [
    ([1, 2, 3, 4, 5], Monotonicity.INCREASING),
    ([5, 6, -10], Monotonicity.NONE),
    ([1, 1, 1, 1], Monotonicity.NONE),
    ([9, 8, 7, 6], Monotonicity.DECREASING),
    ([], Monotonicity.NONE),
    ([1], Monotonicity.NONE),
    ([1, 100], Monotonicity.INCREASING),
    ([1, 100, 100], Monotonicity.NONE),
    ([100, 1], Monotonicity.DECREASING),
    ([100, 1, 1], Monotonicity.NONE),
    ([100, 1, 2], Monotonicity.NONE)
]

for input, expected in tests:
    result = increasing_or_decreasing(input)
    print(f'{result}, {expected}')
    print(result == expected)
