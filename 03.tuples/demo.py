a_tuple = (1, 2, 3)

print(a_tuple[0])

second_way = 1, 2, 3

print(second_way)

items = ['a', 'b', 'c', 'd']
for index in range(len(items)):
    print(index, '->', items[index])
